<?php
/**
 * Wfp\Cart
 *
 * @category  Wfp
 * @package   Wfp\Cart
 * @author    ~~~~~~~~~~~~~
 * @version   1.0.2
 */
namespace Wfp\Cart;

use Illuminate\Session\SessionManager;
use Wfp\Cart\Exceptions\CartInvalidDataException;
use Wfp\Cart\Exceptions\CartInvalidItemQuantityException;
use Wfp\Cart\Exceptions\CartInvalidItemRowIdException;
use Wfp\Cart\Exceptions\CartItemNotFoundException;
use Wfp\Cart\Exceptions\CartRequiredIndexException;

/**
 * Wfp Job Cart Class
 *
 * @package   Wfp\Cart
 * @author    ~~~~~~~~~~~~~
 * @version   1.0.2
 */
class Cart
{
    /**
     * SessionManager Instance
     * @var null|SessionManager
     */
    private $session = null;

    /**
     * WfpJobCart Session Container Name
     * @var null|string
     */
    private $container = null;

    /**
     * WfpJobCart Session Contents
     * @var null
     */
    private $cart_contents = null;

    /**
     * Wfp\Cart Constructor
     *
     * @param SessionManager $session
     */
    public function __construct(SessionManager $session)
    {
        $this->session = $session;
        $this->container = 'WfpJobCart';
        $this->initializeCart();
    }

    /**
     * Wfp\Cart Initialization. Retrieves existing session data if exists
     * and populates the job cart contents with the data. If no data exists
     * the default cart contents are setup.
     *
     * @return Cart
     */
    public function initializeCart()
    {
        // Attempt to set the job cart contents using existing data from the session
        array_set($this->cart_contents, $this->container, $this->session->get($this->container));

        // If there was no existing information, generate default cart entry
        if (is_null(array_get($this->cart_contents, $this->container, null)))
        {
            array_set($this->cart_contents, $this->container, ['total_items' => 0]);
        }

        return $this;
    }


    /**
     * Adds an item to the job cart.
     *
     * @param array $item
     *
     * @return string
     * @throws CartInvalidDataException
     * @throws CartRequiredIndexException
     */
    public function add(array $item = array())
    {
        // Check to see if the quantity is set
        if (!isset($item['qty']))
        {
            $item['qty'] = 1;
        }

        // Validate the item to ensure requirements are met
        $this->validate($item);

        // Verify that the id for the item has been set and
        // then generate SHA1 of the ID to use as the cart row id.
        if (isset($item['id']))
        {
            $rowid = sha1($item['id']);
        }

        // Set the row id for the cart
        $item['rowid'] = $rowid;

        // Update the cart contents container with the new item information
        array_set($this->cart_contents, $this->container . '.' . $rowid, $item);

        // Update the cart in the session
        $this->update_cart();

        // Return the associated row id
        return $rowid;
    }

    /**
     * Validates an item entry
     *
     * @param $item
     *
     * @return Cart
     * @throws CartInvalidDataException
     * @throws CartRequiredIndexException
     */
    protected function validate($item)
    {
        // Ensure that an item was provided and is an array that has data
        if (!is_array($item) or count($item) == 0)
        {
            throw new CartInvalidDataException;
        }

        // Define the required array key indexes for the item
        $required_indexes = array('id', 'title', 'slug', 'qty');

        // Loop through the required indexes
        foreach ($required_indexes as $index)
        {
            // If the required index is not set, throw required index exception
            if (!isset($item[$index]))
            {
                throw new CartRequiredIndexException('Required index [' . $index . '] is missing.');
            }
        }

        return $this;
    }

    /**
     * Synchronizes the job cart session data with the latest local instance of the data.
     *
     * @return bool
     */
    public function update_cart()
    {
        // Reset the cart total items
        array_set($this->cart_contents, $this->container . '.total_items', 0);

        // Update the session with the latest data stored in the cart
        $this->session->put($this->container, array_get($this->cart_contents, $this->container));
        return true;
    }

    /**
     * Updates an existing item in the job cart
     *
     * @param array $item
     *
     * @return bool
     * @throws CartInvalidItemQuantityException
     * @throws CartInvalidItemRowIdException
     * @throws CartItemNotFoundException
     */
    public function update($item = array())
    {
        // Get the current row id for the item and throw exception if not exists
        $rowid = array_get($item, 'rowid', null);
        if (is_null($rowid))
        {
            throw new CartInvalidItemRowIdException;
        }

        // Check to see if the row id exists in the session stored data otherwise
        // throw an exception
        if (is_null(array_get($this->cart_contents, $this->container . '.' . $rowid, null)))
        {
            throw new CartItemNotFoundException;
        }

        // Get the quantity stored and then remove from the array
        $qty = (float)array_get($item, 'qty');
        array_forget($item, 'qty');

        // Remove the rowid from the $item array
        array_forget($item, 'rowid');


        // Verify that the quantity provided is a numeric value or throw exception
        if (!is_numeric($item['qty']))
        {
            throw new CartInvalidItemQuantityException;
        }

        // Verify that there is data provided with the $item and then loop through the data
        // and update the local job cart contents with the new data.
        if (!empty($item))
        {
            foreach ($item as $key => $val)
            {
                array_set($this->cart_contents, $this->container . '.' . $rowid . '.' . $key, $val);
            }
        }

        // Check to see if the quantity has changed. If not then we can safely return at this point
        if (array_get($this->cart_contents, $this->container . '.' . $rowid . '.qty') == $qty)
        {
            return true;
        }

        // If the quantity has been changed in order to request removal, then remove the current
        // item from the job cart contents. Otherwise update the job cart contents with the quantity requested.
        if ($qty <= 0)
        {
            array_forget($this->cart_contents, $this->container . '.' . $rowid);
        }
        else
        {
            array_set($this->cart_contents, $this->container . '.' . $rowid . '.qty', $qty);
        }

        // Call to update the cart session
        $this->update_cart();

        // Return
        return true;
    }

    /**
     * Remove an entry from the job cart session based upon the row id
     *
     * @param string|null $rowid
     *
     * @return bool
     * @throws CartInvalidItemQuantityException
     * @throws CartInvalidItemRowIdException
     * @throws CartItemNotFoundException
     */
    public function remove($rowid = null)
    {
        // Check to see that a row id was provided otherwise throw exception
        if (is_null($rowid))
        {
            throw new CartInvalidItemRowIdException;
        }

        // Call to update the cart and set the quantity for the current rowid to 0
        if ($this->update(array('rowid' => $rowid, 'qty' => 0)))
        {
            return true;
        }
    }

    /**
     * Returns the current job cart contents
     *
     * @return array|null
     */
    public function contents()
    {
        // Retrive the cart contents
        $cart = array_get($this->cart_contents, $this->container);

        // Remove the un-required data
        array_forget($cart, 'total_items');

        // Return the cart contents
        return $cart;
    }

    /**
     * Returns the total number of job postings that exist in the job cart
     *
     * @return mixed
     */
    public function total_items()
    {
        return array_get($this->cart_contents, $this->container . '.total_items', 0);
    }

    /**
     * Destroy the job cart and its associated session
     *
     * @return Cart
     */
    public function destroy()
    {
        // Reset the local job cart container to the default values
        array_set($this->cart_contents, $this->container, array('cart_total' => 0, 'total_items' => 0));

        // Remove the instance of the job cart from the session
        $this->session->forget($this->container);

        // Return to caller
        return Cart;
    }
}