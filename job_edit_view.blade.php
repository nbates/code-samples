@extends('layouts.admin')

{{-- HTML Title --}}
@section('title')
    Edit Job Posting |
    @parent
@stop

{{-- Page Heading --}}
@section('content-header')
    Edit Job Posting
    <small>Job Management</small>
@overwrite
@stop

{{-- Content --}}
@section('content')
    {{ Former::populate($job) }}
    <?= Former::open_for_files()->id('form-job-edit')->method('POST')->action(URL::route('post-job-edit', ['job' => $job->slug]))->secure(); ?>
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-body">
                    <fieldset>
                        <legend>Job Information</legend>
                        <?= Former::xlarge_text('title')->required()->label('Title:') ?>
                        <?= Former::textarea('description')->required()->label('Content Body:')->setAttributes([
                                'class' => 'wysiwyg-editor',
                                'style' => 'width: 100%; min-height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;'
                        ]) ?>
                    </fieldset>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-4">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Job Posting Details</h3>
                </div>
                <div class="box-body">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td class="col-md-6"><strong>Position ID:</strong></td>
                            <td>{{ $job->getPositioningTag() }}</td>
                        </tr>
                        <tr>
                            <td class="col-md-6"><strong>Created On:</strong></td>
                            <td>{{ $job->created_at->toFormattedDateString() }} at {{ $job->created_at->toTimeString() }}</td>
                        </tr>
                        <tr>
                            <td><strong>Last Modified On:</strong></td>
                            <td>{{ $job->updated_at->toFormattedDateString() }} at {{ $job->updated_at->toTimeString() }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="box-header">
                    <h3 class="box-title">Position Details</h3>
                </div>
                <div class="box-body">
                    <?= Former::select('department_type_id')->name('department_type_id')->label('Department:')->fromQuery(Department::all(), 'name', 'id') ?>
                    <?= Former::xlarge_text('active_until')->name('active_until')->label('Application Deadline:')->setAttributes([
                            'data-inputmask' => '\'alias\': \'yyyy-mm-dd\'',
                            'data-mask'      => ''
                    ])->prepend('<i class="fa fa-calendar"></i>') ?>
                    <?= Former::xlarge_text('positions_available')->label('Positions Available:') ?>
                </div>
                <div class="box-header">
                    <h3 class="box-title">Printer Friendly Version</h3>
                </div>
                <div class="box-body">
                    <div id="job-attachment">
                        <table class="table">
                            <tbody>
                            <tr>
                                @if(!empty($job->getAttachedFiles()) && stristr($job->attachment->contentType(), 'pdf'))
                                    <td><span class="fa fa-file-pdf-o fa-2x"></span></td>
                                    <td>{{ HTML::link($job->attachment->url(), $job->attachment->originalFilename(), ['target' => '_blank']) }}</td>
                                    <td><a href="{{ URL::route('post-job-delete-attachment', ['job' => $job->slug]) }}" data-method="post" data-replace="#job-attachment" class="btn btn-default btn-sm ajax"><i class="fa fa-remove"></i></a>
                                    </td>
                                @else
                                    <td>There is no attachment for this Job.</td>
                                @endif
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div style="margin-top: 20px;">
                        <?= Former::file('attachment')->label((!empty($job->getAttachedFiles())) ? 'Replace Print Friendly Version' : 'Upload Print Friendly Version')->accept('application/pdf') ?>
                    </div>
                </div>

                <div class="box-header">
                    <h3 class="box-title">Publish Job Posting</h3>
                </div>
                <div class="box-body">
                    <?= Former::checkbox('is_public')->label(null)->text('  Yes, publish this job posting for viewing.')->check() ?>
                </div>

                <div class="box-body">
                    <hr/>
                    <div class="form-group" style="margin-top: 30px;">
                        <?= Former::actions()->large_primary_submit('Save Changes')->large_inverse_button('Cancel',
                                ['onclick' => 'document.location.href=\'' . URL::to('admin/jobs') . '\'']) ?>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <?= Former::close() ?>
@stop

@section('footer-inline-script')
    <script src="//cdn.ckeditor.com/4.4.6/standard/ckeditor.js"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            $(document).ready(function () {
                CKEDITOR.replace('description', {
                    height: '500'
                });
            });
        }(jQuery));
    </script>
@stop