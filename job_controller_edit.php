<?php
/**
 * AdminJobsController
 *
 * @category  Wfp
 * @package   AdminJobsController
 * @author    ~~~~~~~~~~~~~
 * @version   1.0.5
 */

class AdminJobsController extends \AdminController
{

    /**
     * Job Model
     * @var Job
     */
    protected $job;

    /**
     * Inject the models.
     *
     * @param Job $job
     */
    public function __construct(Job $job)
    {
        parent::__construct();
        $this->job = $job;
    }

    /**
     * Display a listing of the resource.
     * GET /jobs
     *
     * @return Response
     */
    public function getIndex()
    {
        $jobs = Job::where('active_until', '>', Carbon::now()->subDays(3)->format('Y-m-d'))->orderBy('active_until', 'desc')->paginate(20);
        return View::make('admin/jobs/index', ['jobs' => $jobs]);
    }


    /**
     * Show the form for creating a new resource.
     * GET /jobs/create
     *
     * @return Response
     */
    public function getCreate()
    {
        if (!Sentry::getUser()->hasAccess('admin.jobs.create'))
        {
            Session::flash('alert-danger', Lang::get('users.noaccess'));
            App::abort(403, Lang::get('users.noaccess'));
        }
        return View::make('admin/jobs/create');
    }


    /**
     * Store a newly created resource in storage.
     * POST /jobs/create
     *
     * @return Response
     */
    public function postCreate()
    {
        /* proprietary... */
    }


    /**
     * Display the specified resource.
     * GET /jobs/{jobs}
     *
     * @param Job $job
     *
     * @return Response
     */
    public function getShow(Job $job)
    {
        return View::make('admin/jobs/show',
            ['job' => $job]);
    }


    /**
     * Show the form for editing the specified resource.
     * GET /jobs/{jobs}/edit
     *
     * @param Job $job
     *
     * @return Response
     */
    public function getEdit(Job $job)
    {
        return View::make('admin/jobs/edit',
            ['job' => $job]);
    }


    /**
     * Update the specified resource in storage.
     * POST /jobs/{jobs}/edit
     *
     * @param Job $job
     *
     * @return Response
     */
    public function postEdit(Job $job)
    {
        /* proprietary... */

        try
        {
            // Define the validation rules
            $rules = [
                'title'               => 'required',
                'description'         => 'required',
                'wage_type_id'        => 'integer',
                'bu_id'               => 'integer',
                'active_until'        => 'required|date|date_format:Y-m-d',
                'positions_available' => 'integer',
                'is_public'           => 'boolean'
            ];

            // Fetch the input from the request
            $input = Input::get();

            // Validate the date
            $validation = Validator::make($input, $rules);
            if ($validation->fails())
            {
                Session::flash('alert-danger', 'Please correct the errors highlighted below.');
                return Redirect::route('get-job-edit', ['job' => $job->slug])->withInput()
                    ->withErrors($validation);
            }

            // Update the user details
            if ($job->update(Input::all()))
            {
                // Job information was updated
                Session::flash('alert-success', Lang::get('admin/jobs/messages.edit.success'));
                return Redirect::route('get-job-show', ['job' => $job->slug]);
            }

            // Job information was not updated
            Session::flash('alert-danger', Lang::get('admin/jobs/messages.edit.error'));
            return Redirect::route('get-job-edit', ['job' => $job->slug]);

        }
        catch (\Exception $e)
        {
            // Job information was not updated
            Session::flash('alert-danger', Lang::get('admin/jobs/messages.edit.error'));
            return Redirect::route('get-job-edit', ['job' => $job->slug]);
        }
    }


    /**
     * Remove the specified resource from storage.
     * GET /jobs/{jobs}/delete
     *
     * @param Job $job
     *
     * @return Response
     */
    public function getDelete(Job $job)
    {
        /* proprietary... */

        app::abort(403);
    }

    /**
     * Remove the specified resource from storage.
     * POST /jobs/{jobs}/delete
     *
     * @param Job $job
     *
     * @return Response
     */
    public function postDelete(Job $job)
    {
        /* proprietary... */

        app::abort(403);
    }

    /**
     * AJAX Method used to delete attachment for Model Record
     *
     * @param Job $job
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postDeleteAttachment(Job $job)
    {
        $job->attachment = STAPLER_NULL;
        $job->save();
        $response = View::make('admin/jobs/partials/attachment', ['job' => $job])->render();
        return Response::json(['html' => $response]);
    }

}