<?php
Event::listen('user.authenticated', function (User $user, $password)
{
    // Create hashed version of the password
    $hashedPassword = sha1($password);

    // Make sure that the password is not the same as used in the
    // last six months.
    $passwordExpiredCount = UserPasswordsHistory::where('password', '=', $hashedPassword)
        ->where('user_id', '=', $user->id)
        ->where('created_at', '<', DB::raw('date_sub(curdate(), INTERVAL 180 day)'))
        ->count();

    if ($passwordExpiredCount >= 1)
    {
        return Redirect::route('password-expired', ['user' => $user->id]);
    }
});