<?php
// Alpha with Spaces
Validator::extend('alpha_spaces', function ($attribute, $value)
{
    return preg_match('/^[\pL\pM\s]+$/u', $value);
});

// Alpha Numeric with Spaces
Validator::extend('alphanum_spaces', function ($attribute, $value)
{
    return preg_match('/^[\pL\pM\pN\s]+$/u', $value);
});

// Postal and Zip Codes
Validator::extend('postal_zip_codes', function ($attribute, $value)
{
    return preg_match('/^(^\d{5}(-\d{4})?$)|(^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$)/u', $value);
});

// Phone Numbers
Validator::extend('phone_numbers', function ($attribute, $value)
{
    return preg_match('/^((((\(\d{3}\)\s)|(\d{3}-))\d{3}-\d{4}(\s))|(\+?\d{2,3}((-| )\d{1,8}){1,5}))(( x| ext)\d{1,5}){0,1}$/i', $value);
});