<?php
/**
 * Securely compare two strings for equality while avoided C level memcmp()
 * optimisations capable of leaking timing information useful to an attacker
 * attempting to iteratively guess the unknown string (e.g. password) being
 * compared against.
 *
 * @param string $a
 * @param string $b
 *
 * @return bool
 */
function secureStringCompare($a, $b)
{
    if (strlen($a) !== strlen($b))
    {
        return false;
    }
    $result = 0;
    for ($i = 0; $i < strlen($a); $i++)
    {
        $result |= ord($a[$i]) ^ ord($b[$i]);
    }
    return $result == 0;
}

/**
 * Returns a formatted phone number
 *
 * @param $phoneNumber
 *
 * @return mixed|string
 */
function formatPhoneNumber($phoneNumber)
{
    if (!isset($phoneNumber{3}))
    {
        return '';
    }

    $phone = preg_replace('/[^0-9]/', '', $phoneNumber);
    switch (strlen($phone))
    {
        case 7:
            return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
            break;
        case 10:
            return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
            break;
        case 11:
            return preg_replace("/([0-9{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1($2) $3-$4", $phone);
            break;
        default:
            return $phone;
            break;
    }
}

function formatZipCode($zipCode)
{
    if (!isset($zipCode{2}))
    {
        return '';
    }

    $formatted = preg_replace([
        '/([a-zA-Z]{1})([0-9]{1})([a-zA-Z]{1})([0-9]{1})([a-zA-Z]{1})([0-9]{1})/',
        '/([0-9]{5})([0-9]{1,3})/'
    ], [
        "$1$2$3 $4$5$6",
        "$1-$2"
    ], $zipCode);

    return strtoupper($formatted);
}