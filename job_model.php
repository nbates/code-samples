<?php
/**
 * Job Model
 *
 * @category  Wfp
 * @package   Model
 * @author    ~~~~~~~~~~~~~
 * @version   1.0.2
 */

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;

/**
 * Job
 *
 * @package   Model
 * @author    ~~~~~~~~~~~~~
 * @version   1.0.2
 *
 * @property integer          $id
 * @property integer          $bu_id
 * @property integer          $wage_type_id
 * @property string           $title
 * @property string           $description
 * @property integer          $positions_available
 * @property \Carbon\Carbon   $active_until
 * @property boolean          $is_public
 * @property string           $slug
 * @property integer          $position_number
 * @property \Carbon\Carbon   $created_at
 * @property \Carbon\Carbon   $updated_at
 * @property string           $deleted_at
 * @property string           $attachment_file_name
 * @property integer          $attachment_file_size
 * @property string           $attachment_content_type
 * @property string           $attachment_updated_at
 * @property-read \WageType   $wageType
 * @property-read \Department $department
 * @method static \Illuminate\Database\Query\Builder|\Job whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereBuId($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereWageTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Job wherePositionsAvailable($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereActiveUntil($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereIsPublic($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Job wherePositionNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereAttachmentFileName($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereAttachmentFileSize($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereAttachmentContentType($value)
 * @method static \Illuminate\Database\Query\Builder|\Job whereAttachmentUpdatedAt($value)
 */
class Job extends Eloquent implements SluggableInterface, StaplerableInterface
{
    use SluggableTrait, EloquentTrait;

    /**
     * Defines the sluggable field configuration
     * @var array
     */
    protected $sluggable = array(
        'build_from' => 'title',
        'save_to'    => 'slug',
    );

    /**
     * Defines the fillable properties of this model
     * @var array
     */
    protected $fillable = [
        'bu_id',
        'wage_type_id',
        'title',
        'description',
        'positions_available',
        'active_until',
        'attachment',
        'is_public'
    ];

    /**
     * Defines the date columns which will be returned as Carbon objects
     *
     * @var array
     */
    protected $dates = ['active_until'];

    /**
     * Boot
     *
     * @return void
     */
    public static function boot()
    {
        // Register Stapler as an observer for the Job Model
        static::bootStapler();

        // Define the custom process that needs to occur during the model's saving event
        // to ensure that uploaded files have their filenames cleaned and converted to slugs
        // prior to completion of the model save event.
        static::saving(function ($model)
        {
            // Verify that there is an originalFileName associated
            if ($model->attachment->originalFileName() !== null)
            {
                // Get the pathinfo for the file based on the original filename
                $pathInfo = pathinfo($model->attachment->originalFileName());

                // Convert the originalFileName to a slug version
                $newFilename = Str::slug($pathInfo['filename']) . '.' . $pathInfo['extension'];

                // Update the filename attribute in the model
                $model->attachment_file_name = $newFilename;
            }
        });
    }

    /**
     * Model Constructor
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = array())
    {
        // Define that this model has a file attachment associated using Stapler
        $this->hasAttachedFile('attachment', [
            'styles' => []
        ]);

        // Call parent constructor
        parent::__construct($attributes);
    }

    /**
     * Define One-One Relationship
     *
     * @see WageType
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function wageType()
    {
        return $this->hasOne('WageType', 'id', 'wage_type_id');
    }

    /**
     * Define One-One Relationship
     *
     * @see Department
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function department()
    {
        return $this->hasOne('Department', 'id', 'bu_id');
    }

    /**
     * This method will return the next consecutive position number based
     * on the closing date of the current job.
     *
     * @return int
     */
    public function getIncrementedPositionNumber()
    {
        // Set the default lastPosition
        $lastPosition = 1;

        /* proprietary... */

        if ($job instanceof Job)
        {
            $lastPosition = $job->position_number + 1;
        }
        return $lastPosition;
    }

    /**
     * Generates unique position tag for job postings.
     *
     * Schema is as follows:
     *
     *  [Business Unit Number]-[Wage Type]-[Closing Date]-[Increment Based on Closing Date]
     *  eg. 325–2–20130303–2
     *
     * @return string
     */
    public function getPositioningTag()
    {
        // Get the Business Unit Number (Min & Max 3 Characters with leading zeros if required)
        $bu = str_pad($this->department->bu, 3, '0', STR_PAD_LEFT);

        // Get the closing date for the job posting (without the dashes)
        $closureDate = preg_replace('/[\-]/i', '', $this->active_until->toDateString());

        // Return formatted string of the unique positioning tag
        return vsprintf('%s-%s-%s-%s', [$bu, $this->wageType->id, $closureDate, $this->position_number]);
    }
}